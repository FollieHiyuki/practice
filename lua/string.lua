-- Strings are immutable
-- 'modify' it by using another string
local a = 'one string'
local b = string.gsub('one string', 'one', 'another')
print(a)
print(b)

-- Length
print(#"Count the length")

-- C-like escape sequences
print("Backslash inside quotes: '\\'")
print('Backslash inside quotes: \'\\\'')

-- utf-8
print('\x42 is the letter B')
print('\u{3b1} is alpha')

-- Querk
print('Notice the ]] below?')
print [===[a[b[i]] = x]===]
--[====[ Wow comment too
Still inside the comment
]====] print('Normal text here')

-- \z skips all subsequent space characters
local data = '\x00\x01\x02\x03\x04\x05\x06\x07\z
         \x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
print(data)
